#!/usr/bin/env python
import rospy
import time

if __name__ == "__main__":
    
    rospy.init_node('rostime_sleep_test_node', anonymous=True, log_level=rospy.INFO)
    old_clock_time = time.time()
    old_ros_seconds = rospy.get_time()
    
    while not rospy.is_shutdown():
        rospy.sleep(1.0)
        delta_ros_seconds = rospy.get_time() - old_ros_seconds
        rospy.loginfo("ROS Delta time seconds=>"+str(delta_ros_seconds))
        old_ros_seconds = rospy.get_time()
        delta_clock_time = time.time() - old_clock_time
        rospy.loginfo("Delta clock_time=>"+str(delta_clock_time))
        old_clock_time = time.time()
